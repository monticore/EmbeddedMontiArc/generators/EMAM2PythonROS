/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python.EmamGenerator;


import de.monticar.lang.monticar.generator.python.AbstractPythonGenerator;
import de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator.Tagging.RosInterface;
import de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator.Tagging.RosTag;
import de.monticar.lang.monticar.generator.python.TagReader;
import de.monticar.lang.monticar.generator.python.EmamGenerator.blueprints.Component;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.symboltable.Scope;
import org.thymeleaf.context.Context;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmamGenerator extends AbstractPythonGenerator<Component> {

    private List<RosTag> tagList = new ArrayList<>();

    public EmamGenerator(String generationPath){
        super(generationPath, "component");
    }

    public EmamGenerator(){
        super("component");
    }
    public boolean isInComponentList(ExpandedComponentInstanceSymbol symbol){
        return generationInputList.stream()
                .filter(component ->  component.getType().equals(symbol.getComponentType().getFullName()))
                .collect(Collectors.toList())
                .isEmpty();
    };

    public static Stream<ExpandedComponentInstanceSymbol> flattenComponent(ExpandedComponentInstanceSymbol symbol){
        return Stream.concat(
                Stream.of(symbol),
                symbol.getSubComponents().stream().flatMap(EmamGenerator::flattenComponent));

    }
    public void addComponent(ExpandedComponentInstanceSymbol componentSymbol, Scope symtab){
        flattenComponent(componentSymbol)
                .filter(this::isInComponentList)
                .map(cSymbol -> new Component(cSymbol, symtab, HelperFunctions.getInstance()))
                .forEach(component -> generationInputList.add(component));
    }

    @Override
    public String getGenerationFileName(Component input) {
        return input.getType().toLowerCase().replace(".", "/");
    }

    public Context getGenerationContext(Component component){
        Context context = new Context();
        context.setVariable("name", component.getName());
        context.setVariable("parameter", component.getParameter());
        context.setVariable("hasNumpy", component.getHasNumpy());
        context.setVariable("ports", component.getPorts().toArray());
        context.setVariable("instances", component.getInstances());
        context.setVariable("connectors", component.getConnectors());
        context.setVariable("imports", component.getImports());
        context.setVariable("empty", component.getInstances().isEmpty());
        context.setVariable("commands", component.getPythonCommands());
        context.setVariable("portArray", component.getPortArrays());
        return context;
    }
}
