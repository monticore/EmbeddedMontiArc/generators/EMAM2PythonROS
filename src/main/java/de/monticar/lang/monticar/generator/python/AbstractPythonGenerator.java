/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python;

import freemarker.template.Template;
import org.apache.commons.io.FileUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class AbstractPythonGenerator<T> {
    private String generationPath;
    private TemplateEngine templateEngine;
    protected ArrayList<T> generationInputList;
    private String targetTemplateName;

    public AbstractPythonGenerator(String generationPath, String targetTemplateName) {
        generationInputList = new ArrayList<>();
        this.generationPath = generationPath;
        this.targetTemplateName = targetTemplateName;
        templateEngine = new TemplateEngine();
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("/templates/python/");
        templateResolver.setSuffix(".py.tpl");
        templateResolver.setTemplateMode(TemplateMode.TEXT);
        templateResolver.setCharacterEncoding("UTF8");
        templateResolver.setCheckExistence(true);
        templateResolver.setCacheable(false);
        templateEngine.addTemplateResolver(templateResolver);
    }

    public AbstractPythonGenerator(String targetTemplateName) {
        this("target/generated-sources-python/" + LocalDateTime.now().toString(), targetTemplateName);
    }

    public void writeGenerationMapToFiles(HashMap<String, String> generationMap){
        generationMap.forEach((path, componentString) -> {
            try {
                FileUtils.writeStringToFile(new File(path), componentString);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public HashMap<String, String> getGenerationMap() {
        HashMap<String, String> res = new HashMap<>();
        generationInputList.forEach((T input) -> {
            String pythonContent = templateEngine.process(targetTemplateName, getGenerationContext(input));
            String fp = generationPath + "/components/" + getGenerationFileName(input) + ".py";
            res.put(fp, pythonContent);
        });
        return res;
    }
    public void createTargetDirectoryStructure(String commonPath, String libraryPath){
        File f = new File(commonPath);
        File commonDir = new File(this.getClass().getClassLoader().getResource(libraryPath).getPath());
        try {
            FileUtils.copyDirectory(commonDir, f);
            generateInitFiles(f.getParentFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
        f.mkdirs();
    }

    public void generateInitFiles(File target) throws IOException {
        if(target.isDirectory()){
            for (File file: target.listFiles()){
                if (file.isDirectory()){
                    generateInitFiles(file);
                }
            }
        }
        new File(target.getPath() + "/__init__.py").createNewFile();
    }

    public ArrayList<T> getGenerationInputList() {
        return generationInputList;
    }

    public void generate(){
        createTargetDirectoryStructure(generationPath + "/components/common", "templates/python/common");
        HashMap<String, String> generationMap = getGenerationMap();
        writeGenerationMapToFiles(generationMap);
    }

    public void setGenerationInputList(ArrayList<T> generationInputList) {
        this.generationInputList = generationInputList;
    }

    public abstract String getGenerationFileName(T input);

    public abstract Context getGenerationContext(T input);
}
