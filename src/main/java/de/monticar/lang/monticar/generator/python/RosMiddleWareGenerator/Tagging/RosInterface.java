/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator.Tagging;

import java.util.HashMap;

public class RosInterface {
    public String type = "";
    public String topic = "";
    public HashMap<String, String> ports = new HashMap<>();
}
