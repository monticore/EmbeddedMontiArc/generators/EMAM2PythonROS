/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator.Tagging;

import java.util.ArrayList;

public class RosTag {
    public String component = "";
    public ArrayList<RosInterface> subscriber = new ArrayList<>();
    public ArrayList<RosInterface> publisher = new ArrayList<>();
}
