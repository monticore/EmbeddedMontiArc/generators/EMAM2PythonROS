/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python;

import org.thymeleaf.context.Context;

import java.util.ArrayList;

public class MasterGenerator{
    public ArrayList<AbstractPythonGenerator> generators;

    public MasterGenerator(){
        generators = new ArrayList<>();
    }
    public void generate(){
        generators.forEach(AbstractPythonGenerator::generate);
    }
}
