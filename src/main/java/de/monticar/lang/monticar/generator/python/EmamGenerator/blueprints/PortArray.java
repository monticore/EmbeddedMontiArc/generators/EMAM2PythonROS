/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python.EmamGenerator.blueprints;

public class PortArray {
    public Port port;
    public int length = 0;

    public PortArray(Port port) {
        this.port = port;
        length++;
    }

    public boolean isSameType(Port port){
        return this.port.equals(port);
    }

    public void addPort(){
        length++;
    }

}

