/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator;

import de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator.Tagging.RosInterface;
import de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator.Tagging.RosTag;
import de.monticar.lang.monticar.generator.python.MiddlewareGenerator;
import org.thymeleaf.context.Context;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RosMiddlewareGenerator extends MiddlewareGenerator<RosTag>{
    public RosMiddlewareGenerator(String generationPath){
        super(generationPath, "ros_wrapper");
    }

    public RosMiddlewareGenerator(){
        super("ros_wrapper");
    }

    @Override
    public boolean isValidTag(RosTag tag) {
        List<RosTag> duplicates= generationInputList.stream()
                .filter(t -> t.component.equals(tag.component))
                .collect(Collectors.toList());
        return duplicates.isEmpty();
    }

    @Override
    public String getGenerationFileName(RosTag input) {
        return input.component.toLowerCase().replace(".", "/") + "_ros";
    }

    @Override
    public Context getGenerationContext(RosTag input) {
        Context context = new Context();
        context.setVariable("messages", getMessageTypes(input.subscriber, input.publisher));
        context.setVariable("tag", input);
        return context;
    }

    public HashSet<String> getMessageTypes(List<RosInterface> subscriber, List<RosInterface> publisher) {
        HashSet<String> res = new HashSet<>();

        Stream.of(subscriber, publisher)
                .flatMap(Collection::stream)
                .map(e -> e.type)
                .forEach(res::add);

        return res;
    }

}
