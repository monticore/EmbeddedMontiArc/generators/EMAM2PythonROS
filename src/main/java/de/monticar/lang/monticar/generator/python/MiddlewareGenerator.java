/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python;

import de.monticar.lang.monticar.generator.python.TagReader;

import java.util.ArrayList;
import java.util.List;

public abstract class MiddlewareGenerator<T> extends AbstractPythonGenerator<T>{

    public MiddlewareGenerator(String generationPath, String targetTemplateName) {
        super(generationPath, targetTemplateName);
    }

    public MiddlewareGenerator(String targetTemplateName){
        super(targetTemplateName);
    }

    public void addTagsFromPath(String path){
        addTagsFromList(new TagReader<T>().readYAML(path));
    }

    public void addTagsFromList(List<T> tags){
        tags.stream()
                .filter(this::isValidTag)
                .forEach(tag -> generationInputList.add(tag));
    }

    public abstract boolean isValidTag(T tag);


}
