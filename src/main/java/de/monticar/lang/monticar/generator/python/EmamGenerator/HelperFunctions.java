/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python.EmamGenerator;

import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.math.math._symboltable.MathStatementsSymbol;
import de.monticore.lang.monticar.generator.Helper;
import de.monticore.lang.monticar.generator.order.ImplementExecutionOrder;
import de.monticore.lang.tagging._symboltable.TaggingResolver;
import de.monticore.symboltable.Scope;

import java.util.List;

public class HelperFunctions {
    private static HelperFunctions instance;
    private HelperFunctions() {}
    public static HelperFunctions getInstance () {
        if (HelperFunctions.instance == null) {
            HelperFunctions.instance = new HelperFunctions();
        }
        return HelperFunctions.instance;
    }

    public List<ExpandedComponentInstanceSymbol> exOrder(TaggingResolver taggingResolver, ExpandedComponentInstanceSymbol inst) {
        return ImplementExecutionOrder.exOrder(taggingResolver, inst);

    }


    public MathStatementsSymbol getMathStatementsSymbolFor(ExpandedComponentInstanceSymbol symbol, Scope symtab) {
        return  Helper.getMathStatementsSymbolFor(symbol, symtab);
    }
}
