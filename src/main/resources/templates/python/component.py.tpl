[# th:if="${hasNumpy}"]import numpy as np[/]
from components.common.port import Port
[# th:each="import : ${imports.entrySet()}" ]
from components.[(${import.getKey()})] import [(${import.getValue()})]
[/]

class [(${name})]:
    def __init__(self[# th:unless="${parameter.isEmpty()}"],[/][# th:each="entry, iter: ${parameter}"] [(${entry})][# th:unless="${iter.last}"], [/][/]):
    [# th:each="entry, iter: ${parameter}"]
          self.[(${entry})] = [(${entry})]
    [/]
    [# th:each="portArray : ${entry}" ]
            self.[(${entry.port.getName()})] = entry.length * Port([(${entry.port.getValue()})])
    [/] [# th:each="port : ${ports}" ]
        self.[(${port.getName()})] = Port([(${port.getValue()})])
    [/] [# th:each="instance : ${instances}" ]
        self.[(${instance.getName()})] = [(${instance.getType()})]([# th:each="argument, iter : ${instance.getArguments()}"][(${argument})][# th:unless="${iter.last}"], [/][/])
    [/]
    [# th:unless="${connectors.isEmpty()}"]
    def connect(self):
    [# th:each="connector : ${connectors}" ]
        self.[(${connector.getTarget()})] = self.[(${connector.getSource()})]
    [/][/]
    def execute(self): [# th:if="${instances.isEmpty() && commands.isEmpty() && !hasRosWrapper}"]
        pass  [/]
        [# th:each="command : ${commands}" ]
        [(${command})]
        [/]
        [# th:unless="${connectors.isEmpty()}"]self.connect()[/]
        [# th:each="instance : ${instances}" ]
        self.[(${instance.getName()})].execute()
        [/]
