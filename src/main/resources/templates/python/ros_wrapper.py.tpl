import rospy
[# th:each="message : ${messages}"]
from components.messages import [(${message})][/]
[# th:unless="${tag.publisher.isEmpty()}"]import threading
import time
[/]

class [(${name})]Ros:
    def __init__(self, component, freq=1):
        self.component = component
        rospy.init_node()
        [# th:unless="${tag.publisher.isEmpty()}"]
        publisherThread = threading.Thread(target=self.run, args=(freq))
        publisherThread.dameon = True
        publisherThread.start()
        [/]

    [# th:each="pub, iter: ${tag.publisher}" ]
        self.pub[(${iter.count})] = rospy.Publisher('[(${pub.topic})]', [(${pub.type})])
    [/]
    [# th:each="sub, iter: ${tag.subscriber}" ]
        rospy.Subscriber('[(${sub.topic})]', [(${sub.type})], self.handleSub[(${iter.count})])
    [/]
    [# th:each="sub, iter : ${tag.subscriber}" ]
    def handleSub[(${iter.count})](self, msg):
        [# th:each="port: ${sub.ports.entrySet()}" ]
        self.component.[(${port.getKey()})].value = msg.[(${port.getValue()})]
        [/]
    [/]
    [# th:unless="${tag.publisher.isEmpty()}"]
    [# th:each="pub, iter : ${tag.publisher}" ]
    def handlePub[(${iter.count})](self):
        msg = [(${pub.type})]()
    [# th:each="port: ${pub.ports.entrySet()}" ]
        msg.[(${port.getValue()})] = self.component.[(${port.getKey()})].value
        [/]
        return msg
    [/]
    def run(self, freq):
        [# th:each="pub, iter : ${tag.publisher}" ]
        self.pub[(${iter.count})].publish(self.handlePub[(${iter.count})]())
        [/]
        time.sleep(1/freq)
    [/]
