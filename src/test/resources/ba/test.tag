/* (c) https://github.com/MontiCore/monticore */
package ba;

conforms to nfp.PowerConsumptionTagSchema;

tags Delay {
    tag in1 with with PowerConsumption = 20 mW;
}
