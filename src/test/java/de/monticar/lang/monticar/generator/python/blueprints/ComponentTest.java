/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python.blueprints;

import de.monticar.lang.monticar.generator.python.EmamGenerator.blueprints.Component;
import de.monticar.lang.monticar.generator.python.EmamGenerator.HelperFunctions;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentSymbolReference;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConstantPortSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc.unit.constant.EMAConstantValue;
import de.monticore.lang.monticar.ts.references.MCTypeReference;
import de.monticore.symboltable.Scope;
import org.junit.*;
import static org.junit.Assert.*;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ComponentTest {
    @Test
    public void basicComponent() throws Exception {
        Scope mockScope = mock(Scope.class);
        HelperFunctions mockHelper = mock(HelperFunctions.class);
        ExpandedComponentInstanceSymbol mockComponent = mock(ExpandedComponentInstanceSymbol.class);
        ComponentSymbolReference componentSymbolReference = mock(ComponentSymbolReference.class);

        when(componentSymbolReference.getName()).thenReturn("Name");
        when(componentSymbolReference.getFullName()).thenReturn("FullName");
        when(mockComponent.getComponentType()).thenReturn(componentSymbolReference);
        when(mockComponent.getParameters()).thenReturn(new ArrayList<>());
        when(mockHelper.exOrder(any(), any())).thenReturn(new ArrayList<>());
        when(mockComponent.getConnectors()).thenReturn(new ArrayList<>());
        when(mockHelper.getMathStatementsSymbolFor(any(), any())).thenReturn(null);
        Component testComponent = new Component(mockComponent, mockScope, mockHelper);
        assertEquals("Name", testComponent.getName());
        assertEquals("FullName", testComponent.getType());
        assertEquals(0, testComponent.getPorts().size());
        assertEquals(0, testComponent.getInstances().size());
        assertEquals(0, testComponent.getConnectors().size());
        assertEquals(0, testComponent.getParameter().size());
        assertEquals(0, testComponent.getImports().size());
        assertEquals(0, testComponent.getPythonCommands().size());
        assertFalse(testComponent.getHasNumpy());
    }

    @Test
    public void addPortArray() {
        Scope mockScope = mock(Scope.class);
        HelperFunctions mockHelper = mock(HelperFunctions.class);
        ExpandedComponentInstanceSymbol mockComponent = mock(ExpandedComponentInstanceSymbol.class);
        ComponentSymbolReference componentSymbolReference = mock(ComponentSymbolReference.class);

        when(componentSymbolReference.getName()).thenReturn("Name");
        when(componentSymbolReference.getFullName()).thenReturn("FullName");
        when(mockComponent.getComponentType()).thenReturn(componentSymbolReference);
        when(mockComponent.getParameters()).thenReturn(new ArrayList<>());
        when(mockHelper.exOrder(any(), any())).thenReturn(new ArrayList<>());
        when(mockComponent.getConnectors()).thenReturn(new ArrayList<>());
        when(mockHelper.getMathStatementsSymbolFor(any(), any())).thenReturn(null);
        Component testComponent = new Component(mockComponent, mockScope, mockHelper);

        ConstantPortSymbol symbol1 = mock(ConstantPortSymbol.class);
        MCTypeReference mockType = mock(MCTypeReference.class);
        EMAConstantValue const1 = mock(EMAConstantValue.class);
        when(symbol1.getConstantValue()).thenReturn(const1);
        when(const1.getValueAsString()).thenReturn("2/1");
        when(mockType.getName()).thenReturn("Boolean");
        when(symbol1.getTypeReference()).thenReturn(mockType);
        when(symbol1.isConstant()).thenReturn(true);
        when(symbol1.getNameWithoutArrayBracketPart()).thenReturn("port1");

        when(symbol1.isPartOfPortArray()).thenReturn(true);
        testComponent.addPort(symbol1);
        assertEquals(1, testComponent.getPortArrays().size());
        testComponent.addPort(symbol1);
        assertEquals(1, testComponent.getPortArrays().size());
        assertEquals(0, testComponent.getPorts().size());
        when(symbol1.getNameWithoutArrayBracketPart()).thenReturn("random");
        testComponent.addPort(symbol1);
        assertEquals(2, testComponent.getPortArrays().size());
        when(symbol1.isPartOfPortArray()).thenReturn(false);
        testComponent.addPort(symbol1);
        assertEquals(1, testComponent.getPorts().size());
    }
}
