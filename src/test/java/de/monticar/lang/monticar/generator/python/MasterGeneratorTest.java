/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python;

import de.monticar.lang.monticar.generator.python.EmamGenerator.EmamGenerator;
import de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator.RosMiddlewareGenerator;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.symboltable.Scope;
import org.junit.Test;

import static de.monticore.lang.monticar.generator.order.simulator.AbstractSymtab.createSymTab;
import static org.junit.Assert.*;

public class MasterGeneratorTest {
    @Test
    public void generate() throws Exception {
        EmamGenerator emamGenerator = new EmamGenerator("test");
        Scope symtab = createSymTab("src/test/resources");
        ExpandedComponentInstanceSymbol componentSymbol = symtab.<ExpandedComponentInstanceSymbol>resolve("verteidigung.matrixExample", ExpandedComponentInstanceSymbol.KIND).orElse(null);
        emamGenerator.addComponent(componentSymbol, symtab);
        MasterGenerator master = new MasterGenerator();
        master.generators.add(emamGenerator);
        RosMiddlewareGenerator rosMiddlewareGenerator = new RosMiddlewareGenerator("test");
        rosMiddlewareGenerator.addTagsFromPath(this.getClass().getClassLoader().getResource("verteidigung/config.yaml").getPath());
        master.generators.add(rosMiddlewareGenerator);
        master.generate();


    }

}
