/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python;

import static org.mockito.Mockito.*;

import de.monticar.lang.monticar.generator.python.EmamGenerator.ConversionHelper;
import de.monticar.lang.monticar.generator.python.EmamGenerator.MathExpressionPython;
import de.monticore.lang.math.math._symboltable.JSValue;
import de.monticore.lang.math.math._symboltable.MathAssignmentOperator;
import de.monticore.lang.math.math._symboltable.MathForLoopHeadSymbol;
import de.monticore.lang.math.math._symboltable.expression.*;
import de.monticore.lang.math.math._symboltable.matrix.MathMatrixExpressionSymbol;
import de.monticore.lang.math.math._symboltable.matrix.MathMatrixNameExpressionSymbol;
import de.monticore.lang.math.math._symboltable.matrix.MathMatrixVectorExpressionSymbol;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class ConversionHelperTest {
    @Test
    public void mathSymbolToNumpy() throws Exception {
        String mat1 = "[(12 222 3333; 4 55 6; 7 8 9)]";
        String res = ConversionHelper.mathSymbolToNumpy(mat1);
        assertEquals("np.array([12,222,3333,4,55,6,7,8,9]).reshape(3,3)", res);
    }

    @Test
    public void getMatrix() throws Exception {
        String mat1 = "[(12 222 3333; 4 55 6; 7 8 9)]";
        String mat2 = "[(12,222, 3333; 4,55,6; 7 , 8, 9)]";

        ArrayList<ArrayList<String>> resMat1 = ConversionHelper.getMatrix(mat1.toCharArray());
        ArrayList<ArrayList<String>> resMat2 = ConversionHelper.getMatrix(mat2.toCharArray());

        ArrayList<ArrayList<String>> resMat3 = new ArrayList<ArrayList<String>>();
        ArrayList<String> row1 = new ArrayList<>();
        ArrayList<String> row2 = new ArrayList<>();
        ArrayList<String> row3 = new ArrayList<>();

        row1.add("12");
        row1.add("222");
        row1.add("3333");
        row2.add("4");
        row2.add("55");
        row2.add("6");
        row3.add("7");
        row3.add("8");
        row3.add("9");

        resMat3.add(row1);
        resMat3.add(row2);
        resMat3.add(row3);

        assertEquals(resMat1, resMat3);
        assertEquals(resMat2, resMat3);
    }

    @Test
    public void getExpressionType() throws Exception {
        MathForLoopExpressionSymbol mathForLoopExpressionSymbol = new MathForLoopExpressionSymbol();
        MathAssignmentExpressionSymbol mathAssignmentExpressionSymbol = new MathAssignmentExpressionSymbol();
        MathArithmeticExpressionSymbol  mathArithmeticExpressionSymbol = new MathArithmeticExpressionSymbol();
        MathConditionalExpressionsSymbol mathConditionalExpressionsSymbol = new MathConditionalExpressionsSymbol();
        MathCompareExpressionSymbol mathCompareExpressionSymbol = new MathCompareExpressionSymbol();
        MathNumberExpressionSymbol mathValueExpressionSymbol = new MathNumberExpressionSymbol();
        MathMatrixExpressionSymbol mathMatrixExpressionSymbol = new MathMatrixNameExpressionSymbol("testMat");
        MathExpressionSymbol unknownType = new MathExpressionSymbol() {
            @Override
            public String getTextualRepresentation() {
                return null;
            }
        };

        mathValueExpressionSymbol.setValue(new JSValue());

        assertEquals(MathExpressionPython.FOR, ConversionHelper.getExpressionType(mathForLoopExpressionSymbol));
        assertEquals(MathExpressionPython.ASSIGNMENT, ConversionHelper.getExpressionType(mathAssignmentExpressionSymbol));
        assertEquals(MathExpressionPython.ARITHMETIC, ConversionHelper.getExpressionType(mathArithmeticExpressionSymbol));
        assertEquals(MathExpressionPython.CONDITIONAL, ConversionHelper.getExpressionType(mathConditionalExpressionsSymbol));
        assertEquals(MathExpressionPython.COMPARE, ConversionHelper.getExpressionType(mathCompareExpressionSymbol));
        assertEquals(MathExpressionPython.VALUE, ConversionHelper.getExpressionType(mathValueExpressionSymbol));
        assertEquals(MathExpressionPython.MATRIX, ConversionHelper.getExpressionType(mathMatrixExpressionSymbol));
        assertEquals(MathExpressionPython.UNKNOWN, ConversionHelper.getExpressionType(unknownType));

    }

    @Test
    public void isPortName() throws Exception {
        List<String> testNames = new ArrayList<>();
        String testName = "testPort";
        testNames.add(testName);
        ConversionHelper.setNames(testNames);
        assertTrue(ConversionHelper.isPortName(testName));
        ConversionHelper.setNames(new ArrayList<>());
        assertFalse(ConversionHelper.isPortName(testName));
    }

    @Test
    public void getForBehaviour() throws Exception {
        MathForLoopExpressionSymbol forLoopExpressionSymbol = new MathForLoopExpressionSymbol();
        MathForLoopHeadSymbol forLoopHeadSymbol = new MathForLoopHeadSymbol();
        MathCompareExpressionSymbol mathCompareExpressionSymbol = new MathCompareExpressionSymbol();
        MathNumberExpressionSymbol mathNumberExpressionSymbol = new MathNumberExpressionSymbol();
        mathNumberExpressionSymbol.setValue(new JSValue());
        mathCompareExpressionSymbol.setLeftExpression(mathNumberExpressionSymbol);
        mathCompareExpressionSymbol.setCompareOperator("<");
        mathCompareExpressionSymbol.setRightExpression(mathNumberExpressionSymbol);
        forLoopExpressionSymbol.setForLoopHead(forLoopHeadSymbol);
        forLoopExpressionSymbol.addForLoopBody(mathCompareExpressionSymbol);
        MathMatrixVectorExpressionSymbol vec = new MathMatrixVectorExpressionSymbol();
        vec.setStart(mathNumberExpressionSymbol);
        vec.setEnd(mathNumberExpressionSymbol);
        vec.setStep(mathNumberExpressionSymbol);
        forLoopHeadSymbol.setMathExpression(vec);
        forLoopHeadSymbol.setNameLoopVariable("i");
        String[] pythonCommands = ConversionHelper.getMathBehaviour(forLoopExpressionSymbol).split(";");
        assertEquals("for i in range(1, 1, 1):", pythonCommands[0]);
        assertEquals("    1<1", pythonCommands[1]);
    }

    @Test
    public void getCompareBehaviour(){
        MathNumberExpressionSymbol mathNumberExpressionSymbol = new MathNumberExpressionSymbol();
        MathCompareExpressionSymbol mathCompareExpressionSymbol = new MathCompareExpressionSymbol();
        mathCompareExpressionSymbol.setLeftExpression(mathNumberExpressionSymbol);
        mathCompareExpressionSymbol.setCompareOperator("<");
        mathCompareExpressionSymbol.setRightExpression(mathNumberExpressionSymbol);
        mathNumberExpressionSymbol.setValue(new JSValue());
        String[] pythonCommands = ConversionHelper.getMathBehaviour(mathCompareExpressionSymbol).split(";");
        assertEquals("1<1", pythonCommands[0]);
    }

    @Test
    public void getValueBehaviour(){
        MathNumberExpressionSymbol mathNumberExpressionSymbol = new MathNumberExpressionSymbol();
        mathNumberExpressionSymbol.setValue(new JSValue());
        assertEquals("1", ConversionHelper.getMathBehaviour(mathNumberExpressionSymbol));
        MathNameExpressionSymbol portNameExpression = new MathNameExpressionSymbol();
        portNameExpression.setNameToAccess("port1");
        assertEquals("self.port1", ConversionHelper.getMathBehaviour(portNameExpression));

    }

    @Test
    public void getAssignmentBehaviour(){
        MathNumberExpressionSymbol mathNumberExpressionSymbol = new MathNumberExpressionSymbol();
        mathNumberExpressionSymbol.setValue(new JSValue());
        MathAssignmentExpressionSymbol mathAssignmentExpressionSymbol = new MathAssignmentExpressionSymbol();
        mathAssignmentExpressionSymbol.setAssignmentOperator(new MathAssignmentOperator("="));
        mathAssignmentExpressionSymbol.setNameOfMathValue("test");
        mathAssignmentExpressionSymbol.setExpressionSymbol(mathNumberExpressionSymbol);
        assertEquals("self.test=1;", ConversionHelper.getMathBehaviour(mathAssignmentExpressionSymbol));

    }

    @Test
    public void getArithmeticBehaviour(){
        MathNumberExpressionSymbol mathNumberExpressionSymbol = new MathNumberExpressionSymbol();
        mathNumberExpressionSymbol.setValue(new JSValue());
        MathArithmeticExpressionSymbol mathArithmeticExpressionSymbol = new MathArithmeticExpressionSymbol();
        mathArithmeticExpressionSymbol.setLeftExpression(mathNumberExpressionSymbol);
        mathArithmeticExpressionSymbol.setRightExpression(mathNumberExpressionSymbol);
        mathArithmeticExpressionSymbol.setOperator("+");
        assertEquals("1+1", ConversionHelper.getMathBehaviour(mathArithmeticExpressionSymbol));

    }

    @Test
    public void getMarixBehaviour(){
        MathMatrixExpressionSymbol matMock = mock(MathMatrixExpressionSymbol.class);
        when(matMock.isMatrixNameExpression()).thenReturn(false);
        when(matMock.getTextualRepresentation()).thenReturn("[(12 222 3333; 4 55 6; 7 8 9)]");
        when(matMock.isMatrixExpression()).thenReturn(true);
        assertEquals("np.array([12,222,3333,4,55,6,7,8,9]).reshape(3,3)", ConversionHelper.getMathBehaviour(matMock));
    }

    @Test
    public void getConditionalBehaviour() {
        MathConditionalExpressionsSymbol mathConditionalExpressionsSymbol = new MathConditionalExpressionsSymbol();
        MathConditionalExpressionSymbol ifStatement = new MathConditionalExpressionSymbol();
        MathCompareExpressionSymbol mathCompareExpressionSymbol = new MathCompareExpressionSymbol();
        MathNumberExpressionSymbol mathNumberExpressionSymbol = new MathNumberExpressionSymbol();
        mathNumberExpressionSymbol.setValue(new JSValue());
        mathCompareExpressionSymbol.setLeftExpression(mathNumberExpressionSymbol);
        mathCompareExpressionSymbol.setCompareOperator("<");
        mathCompareExpressionSymbol.setRightExpression(mathNumberExpressionSymbol);
        ifStatement.addBodyExpression(mathCompareExpressionSymbol);
        mathConditionalExpressionsSymbol.setIfConditionalExpression(ifStatement);
        ifStatement.setCondition(mathCompareExpressionSymbol);
        mathConditionalExpressionsSymbol.setElseConditionalExpression(ifStatement);
        String[] res = ConversionHelper.getMathBehaviour(mathConditionalExpressionsSymbol).split(";");
        assertEquals("if (1<1): ", res[0]);
        assertEquals("    1<1", res[1]);
        assertEquals("else: ", res[2]);
        assertEquals("    1<1", res[3]);

    }

    @Test
    public void getPortBehaviour(){
        List<String> testNames = new ArrayList<>();
        String testName = "testPort";
        testNames.add(testName);
        ConversionHelper.setNames(testNames);
        MathValueExpressionSymbol port = mock(MathNumberExpressionSymbol.class);
        when(port.getTextualRepresentation()).thenReturn("testPort");
        when(port.isValueExpression()).thenReturn(true);
        assertEquals("self.testPort.value", ConversionHelper.getMathBehaviour(port));

    }

    @Test
    public void getUnkownBehaviour(){
        MathExpressionSymbol unknownType = new MathExpressionSymbol() {
            @Override
            public String getTextualRepresentation() {
                return null;
            }
        };
        assertEquals("", ConversionHelper.getMathBehaviour(unknownType));
    }

    @Test
    public void setNames() throws Exception {
    }

}
