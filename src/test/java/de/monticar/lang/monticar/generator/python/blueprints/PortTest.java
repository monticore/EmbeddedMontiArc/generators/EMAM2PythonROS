/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python.blueprints;

import de.monticar.lang.monticar.generator.python.EmamGenerator.blueprints.Port;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConstantPortSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.PortSymbol;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc.unit.constant.EMAConstantValue;
import de.monticore.lang.monticar.ts.references.MCTypeReference;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PortTest {
    @Test
    public void getValue() throws Exception {
        ConstantPortSymbol symbol1 = mock(ConstantPortSymbol.class);
        PortSymbol symbol2 = mock(PortSymbol.class);
        MCTypeReference mockType = mock(MCTypeReference.class);
        EMAConstantValue const1 = mock(EMAConstantValue.class);
        when(symbol1.getConstantValue()).thenReturn(const1);
        when(const1.getValueAsString()).thenReturn("2/1");
        when(mockType.getName()).thenReturn("Boolean");
        when(symbol1.getTypeReference()).thenReturn(mockType);
        when(symbol1.isConstant()).thenReturn(true);
        when(symbol1.getNameWithoutArrayBracketPart()).thenReturn("port1");
        Port port1 = new Port(symbol1);

        when(symbol2.getTypeReference()).thenReturn(mockType);
        when(symbol2.getNameWithoutArrayBracketPart()).thenReturn("port1");
        Port port2 = new Port(symbol2);

        assertEquals("2/1", port1.getValue());
        assertEquals("False", port2.getValue());
    }

    @Test
    public void equals() throws Exception {
        PortSymbol symbol1 = mock(PortSymbol.class);
        PortSymbol symbol2 = mock(PortSymbol.class);
        PortSymbol symbol3 = mock(PortSymbol.class);
        MCTypeReference mockType = mock(MCTypeReference.class);
        when(mockType.getName()).thenReturn("Boolean");
        when(symbol1.getTypeReference()).thenReturn(mockType);
        when(symbol1.getNameWithoutArrayBracketPart()).thenReturn("port1");
        when(symbol2.getTypeReference()).thenReturn(mockType);
        when(symbol2.getNameWithoutArrayBracketPart()).thenReturn("port1");
        when(symbol3.getNameWithoutArrayBracketPart()).thenReturn("port2");
        when(symbol3.getTypeReference()).thenReturn(mockType);
        Port port1 = new Port(symbol1);
        Port port2 = new Port(symbol2);
        Port port3 = new Port(symbol3);
        assertTrue(port1.equals(port2));
        assertFalse(port3.equals(port2));
    }
}
