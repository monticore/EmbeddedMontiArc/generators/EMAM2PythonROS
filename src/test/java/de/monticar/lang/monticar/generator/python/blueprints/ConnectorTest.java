/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python.blueprints;

import de.monticar.lang.monticar.generator.python.EmamGenerator.blueprints.Connector;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ConnectorSymbol;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConnectorTest {
    @Test
    public void constructBasicConnector() throws Exception{
        ConnectorSymbol connectorSymbol = mock(ConnectorSymbol.class);
        when(connectorSymbol.getSource()).thenReturn("source");
        when(connectorSymbol.getTarget()).thenReturn("target");
        Connector connector = new Connector(connectorSymbol);
        assertEquals("source", connector.getSource());
        assertEquals("target", connector.getTarget());
    }

}
