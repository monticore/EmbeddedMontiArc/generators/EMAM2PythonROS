/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python.blueprints;

import de.monticar.lang.monticar.generator.python.EmamGenerator.blueprints.Component;
import de.monticar.lang.monticar.generator.python.EmamGenerator.blueprints.Instance;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ComponentSymbolReference;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.lang.math.math._symboltable.expression.MathExpressionSymbol;
import de.monticore.lang.monticar.mcexpressions._ast.ASTExpression;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class InstanceTest {

    @Test
    public void getArguments() throws Exception {
        ExpandedComponentInstanceSymbol mockInstance = mock(ExpandedComponentInstanceSymbol.class);
        Component mockComponent = mock(Component.class);
        ComponentSymbolReference mockReference = mock(ComponentSymbolReference.class);
        ASTExpression argument1 = mock(ASTExpression.class);
        ASTExpression argument2 = mock(ASTExpression.class);
        ASTExpression argument3 = mock(ASTExpression.class);
        MathExpressionSymbol nonMatrixSymbol = mock(MathExpressionSymbol.class);
        MathExpressionSymbol mathMatrixAccessOperatorSymbol = mock(MathExpressionSymbol.class);

        ArrayList<ASTExpression> argumentList = new ArrayList<>();
        //argumentList.add((ASTExpression) argument1);
        //argumentList.add((ASTExpression) argument2);
        when(mockReference.getFullName()).thenReturn("package1.component1");
        when(mockReference.getName()).thenReturn("component1");
        when(mockInstance.getComponentType()).thenReturn(mockReference);
        when(mockInstance.getName()).thenReturn("component1");
        when(mathMatrixAccessOperatorSymbol.isMatrixExpression()).thenReturn(true);
        when(mathMatrixAccessOperatorSymbol.getTextualRepresentation()).thenReturn("[1 1]");
        when(argument1.toString()).thenReturn("undefined");
        when(argument1.getSymbol()).thenReturn(Optional.empty());
        when(nonMatrixSymbol.isMatrixExpression()).thenReturn(false);
        when(nonMatrixSymbol.getTextualRepresentation()).thenReturn("1");

        doReturn(Optional.of(mathMatrixAccessOperatorSymbol)).when(argument2).getSymbol();
        doReturn(Optional.of(nonMatrixSymbol)).when(argument3).getSymbol();

        Instance testInstance = new Instance(mockInstance, mockComponent);

        testInstance.addArgument(argument1);
        testInstance.addArgument(argument2);
        testInstance.addArgument(argument3);

        assertEquals(3, testInstance.getArguments().size());
        assertEquals("component1", testInstance.getName());
        assertEquals("component1", testInstance.getType());
        assertEquals("package1.component1", testInstance.getPath());
    }

}
