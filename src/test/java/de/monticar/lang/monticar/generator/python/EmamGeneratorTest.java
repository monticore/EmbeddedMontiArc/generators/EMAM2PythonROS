/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python;

import de.monticar.lang.monticar.generator.python.EmamGenerator.blueprints.Component;
import de.monticar.lang.monticar.generator.python.EmamGenerator.EmamGenerator;
import de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator.Tagging.RosInterface;
import de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator.Tagging.RosTag;
import de.monticore.lang.embeddedmontiarc.embeddedmontiarc._symboltable.ExpandedComponentInstanceSymbol;
import de.monticore.symboltable.Scope;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.NameFileFilter;
import org.junit.Test;
import org.thymeleaf.context.Context;
import java.io.File;
import java.util.*;

import static de.monticore.lang.monticar.generator.order.simulator.AbstractSymtab.createSymTab;
import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EmamGeneratorTest {

    @Test
    public void addComponent() throws Exception {
        Scope symtab = createSymTab("src/test/resources");
        ExpandedComponentInstanceSymbol componentSymbol = symtab.<ExpandedComponentInstanceSymbol>resolve("test.lookUpInstance", ExpandedComponentInstanceSymbol.KIND).orElse(null);
        assertNotNull(componentSymbol);
        EmamGenerator emamGenerator = new EmamGenerator();
        emamGenerator.addComponent(componentSymbol, symtab);
        ArrayList<Component> componentList = emamGenerator.getGenerationInputList();
        assertNotNull(componentList);
        assertEquals(2, componentList.toArray().length);
    }

    /*
    TO-DO
    @Test
    public void getTag() throws Exception {
        EmamGenerator emamGenerator = new EmamGenerator();
        Component com1 = mock(Component.class);
        Component com2 = mock(Component.class);
        when(com1.getType()).thenReturn("mock1");
        when(com2.getType()).thenReturn("mock2");
        ArrayList<Component> componentList = emamGenerator.getGenerationInputList();
        componentList.add(com1);
        componentList.add(com2);

        ArrayList<RosTag> tagList = new ArrayList<>();
        RosTag tag1 = new RosTag();
        RosTag tag2 = new RosTag();
        tag1.component = "mock1";
        tag2.component = "unknown";
        tagList.add(tag1);
        tagList.add(tag2);
        emamGenerator.setTagList(tagList);

        Optional<RosTag> res1 = emamGenerator.getTag(com1);
        Optional<RosTag> res2 = emamGenerator.getTag(com2);
        tag2.component = "mock2";
        Optional<RosTag> res3 = emamGenerator.getTag(com2);

        assertTrue(res1.isPresent());
        assertFalse(res2.isPresent());
        assertTrue(res3.isPresent());
    }
    */

    @Test
    public void generate() throws Exception {
        ArrayList<Component> componentList = new ArrayList<>();
        Component componentMock = mock(Component.class);
        componentList.add(componentMock);
        when(componentMock.getType()).thenReturn("mock");
        EmamGenerator emamGenerator = new EmamGenerator("test");

        emamGenerator.setGenerationInputList(componentList);
        emamGenerator.generate();
    }

    @Test
    public void getGenerationMap() throws Exception {
        ArrayList<Component> componentList = new ArrayList<>();
        Component componentMock = mock(Component.class);
        componentList.add(componentMock);
        when(componentMock.getType()).thenReturn("mock");
        EmamGenerator emamGenerator = new EmamGenerator("test");;
        emamGenerator.setGenerationInputList(componentList);
        HashMap<String, String> res = emamGenerator.getGenerationMap();
        assertEquals(1, res.size());
        assertEquals("test/components/mock.py", res.entrySet().iterator().next().getKey());
        assertNotEquals("", res.entrySet().iterator().next().getValue());
    }

    @Test
    public void createTargetDirectoryStructure() throws Exception{
        String commonPath = "target/testDirectory/common";
        String libraryPath = "templates/python/common";
        String fakeLibraryPath = "fakeLibrary";
        EmamGenerator emamGenerator = new EmamGenerator();
        emamGenerator.createTargetDirectoryStructure(commonPath, libraryPath);
        assertTrue(new File(commonPath).exists());
        assertTrue(new File(commonPath).isDirectory());
        assertTrue(new File(commonPath).listFiles().length > 0);
        FileUtils.deleteDirectory(new File("target/testDirectory"));
    }

    @Test
    public void writeGenerationMapToFiles() throws Exception{
        HashMap<String, String> generationMap = new HashMap<>();
        String componentPath = "target/testDirectory/testComponent";
        generationMap.put(componentPath, "testComponent");
        new EmamGenerator().writeGenerationMapToFiles(generationMap);
        assertTrue(new File(componentPath).exists());
        assertEquals("testComponent", FileUtils.readFileToString(new File(componentPath)));
        FileUtils.deleteDirectory(new File("target/testDirectory"));
    }

    @Test
    public void getComponentContext() throws Exception{
        EmamGenerator emamGenerator = new EmamGenerator();
        Component component = mock(Component.class);
        when(component.getName()).thenReturn("ab");

        ArrayList<String> parameter = new ArrayList<>();
        when(component.getParameter()).thenReturn(parameter);
        Context context = emamGenerator.getGenerationContext(component);
        assertEquals("ab", context.getVariable("name"));
    }
    @Test
    public void generateInitFiles() throws Exception {
        File testDirectory = new File("target/fileTest/mock1/mock2/");
        Boolean created = testDirectory.mkdirs();
        assertTrue(created);
        EmamGenerator emamGenerator = new EmamGenerator();
        File mockDirectory1 = new File("target/fileTest/mock1");
        File mockDirectory2 = new File("target/fileTest/mock1/mock2");
        File mockFile = new File("target/fileTest/mock1/mock2/mock");
        emamGenerator.generateInitFiles(mockDirectory1);
        Collection<File> res1 = FileUtils.listFiles(mockDirectory1, new NameFileFilter("__init__.py"), null);
        Collection<File> res2 = FileUtils.listFiles(mockDirectory2, new NameFileFilter("__init__.py"), null);

        assertEquals(1, res1.size());
        assertEquals(1, res2.size());
        FileUtils.deleteDirectory(new File("target/fileTest"));

    }

}
