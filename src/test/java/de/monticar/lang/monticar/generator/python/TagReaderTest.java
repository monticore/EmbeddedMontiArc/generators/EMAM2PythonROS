/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python;

import de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator.Tagging.RosTag;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.*;

public class TagReaderTest {
    @Test
    public void readYAML() throws Exception {
        TagReader<RosTag> reader = new TagReader<>();
        List<RosTag> tags = reader.readYAML(this.getClass().getClassLoader().getResource("autocart/tags/config.yaml").getPath());
        assertNotNull(tags);
        assertEquals(tags.get(0).component, "ba.Delay");
        assertEquals(tags.get(0).subscriber.size(), 2);
        assertEquals(0, reader.readYAML("randomPath/random").size());
    }
}
