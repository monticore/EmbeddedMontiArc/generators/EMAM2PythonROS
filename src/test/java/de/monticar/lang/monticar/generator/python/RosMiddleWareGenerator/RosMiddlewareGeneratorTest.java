/* (c) https://github.com/MontiCore/monticore */
package de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator;

import de.monticar.lang.monticar.generator.python.EmamGenerator.EmamGenerator;
import de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator.Tagging.RosInterface;
import de.monticar.lang.monticar.generator.python.RosMiddleWareGenerator.Tagging.RosTag;
import org.junit.Test;
import org.thymeleaf.context.Context;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.*;

public class RosMiddlewareGeneratorTest {

    @Test
    public void addTagsFromPath() throws Exception {
        RosMiddlewareGenerator rosMiddlewareGenerator = new RosMiddlewareGenerator();
        rosMiddlewareGenerator.addTagsFromPath(this.getClass().getClassLoader().getResource("autocart/tags/config.yaml").getPath());
        assertEquals(rosMiddlewareGenerator.getGenerationInputList().get(0).component, "ba.Delay");
        assertEquals(rosMiddlewareGenerator.getGenerationInputList().get(0).subscriber.size(), 2);
    }

    @Test
    public void addTagsFromList() throws Exception {
        RosMiddlewareGenerator generator = new RosMiddlewareGenerator();
        RosTag tag1 = new RosTag();
        tag1.component = "ba.Component";
        RosTag tag2 = new RosTag();
        tag2.component = "ab.Component";
        RosTag tag3 = new RosTag();
        tag3.component = "baab.Component";
        ArrayList<RosTag> tags1 = new ArrayList<>();
        ArrayList<RosTag> tags2 = new ArrayList<>();
        tags1.add(tag1);
        tags1.add(tag2);
        tags2.add(tag1);
        tags2.add(tag3);
        generator.addTagsFromList(tags1);
        assertEquals(generator.getGenerationInputList().size(), 2);
        generator.addTagsFromList(tags2);
        assertEquals(generator.getGenerationInputList().size(), 3);
    }

    @Test
    public void isValidTag() throws Exception {
        RosMiddlewareGenerator rosMiddlewareGenerator = new RosMiddlewareGenerator();
        RosTag tag1 = new RosTag();
        tag1.component = "ba.Component";
        RosTag tag2 = new RosTag();
        tag2.component = "ba.Component";
        RosTag tag3 = new RosTag();
        tag3.component = "Component";
        assertEquals(rosMiddlewareGenerator.isValidTag(tag1), true);
        rosMiddlewareGenerator.getGenerationInputList().add(tag1);
        assertEquals(rosMiddlewareGenerator.isValidTag(tag1), false);
        assertEquals(rosMiddlewareGenerator.isValidTag(tag2), false);
        assertEquals(rosMiddlewareGenerator.isValidTag(tag3), true);
    }

    @Test
    public void getGenerationFileName() throws Exception {
    }

    @Test
    public void getGenerationContext() throws Exception {
        RosMiddlewareGenerator rosMiddlewareGenerator = new RosMiddlewareGenerator();

        RosTag tag1 = new RosTag();
        tag1.component = "rosComponent";
        RosInterface sub1 = new RosInterface();
        sub1.type = "sub";
        tag1.subscriber.add(sub1);
        ArrayList<RosTag> tagList = new ArrayList<>();
        tagList.add(tag1);
        Context rosComponentContext = rosMiddlewareGenerator.getGenerationContext(tag1);
        assertNotEquals(new ArrayList<>(), rosComponentContext.getVariable("messages"));
        assertEquals(tag1, rosComponentContext.getVariable("tag"));
    }

    @Test
    public void getMessageTypes() throws Exception {
        ArrayList<RosInterface> subscribers = new ArrayList<>();
        ArrayList<RosInterface> publishers = new ArrayList<>();
        RosInterface sub1 = new RosInterface();
        sub1.type = "type1";
        RosInterface sub2 = new RosInterface();
        sub2.type = "type2";
        RosInterface sub3 = new RosInterface();
        sub3.type = "type2";
        RosInterface pub1 = new RosInterface();
        pub1.type = "type1";
        subscribers.add(sub1);
        subscribers.add(sub2);
        subscribers.add(sub3);
        publishers.add(pub1);

        RosMiddlewareGenerator emamGenerator = new RosMiddlewareGenerator();
        HashSet<String> res = emamGenerator.getMessageTypes(subscribers, publishers);
        HashSet<String> expected = new HashSet<>();
        expected.add("type1");
        expected.add("type2");
        assertTrue(expected.equals(res));
    }

}
