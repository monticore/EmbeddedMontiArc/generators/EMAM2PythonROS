<!-- (c) https://github.com/MontiCore/monticore -->
[![Build Status](https://travis-ci.org/EmbeddedMontiArc/EMAM2PythonROS.svg?branch=master)](https://travis-ci.org/EmbeddedMontiArc/EMAM2PythonROS)
[![Build Status](https://circleci.com/gh/EmbeddedMontiArc/EMAM2PythonROS/tree/master.svg?style=shield&circle-token=:circle-token)](https://circleci.com/gh/EmbeddedMontiArc/EMAM2PythonROS/tree/master)
[![Maintainability](https://api.codeclimate.com/v1/badges/7d0f1a2508ead035c875/maintainability)](https://codeclimate.com/github/EmbeddedMontiArc/EMAM2PythonROS/maintainability)
[![Coverage Status](https://coveralls.io/repos/github/EmbeddedMontiArc/EMAM2PythonROS/badge.svg?branch=master)](https://coveralls.io/github/EmbeddedMontiArc/EMAM2PythonROS?branch=master)

![Logo of the project](https://avatars1.githubusercontent.com/u/30497665?s=200&v=4)

# EMAM2PythonROS
> Embedded Montiarc Math to Python generator with an optional ROS wrapper

-- To Do --
